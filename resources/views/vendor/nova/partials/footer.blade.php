<p class="mt-8 text-center text-xs text-80">
    <a href="https://nova.laravel.com" class="text-primary dim no-underline">Aulab Srl</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Questo è il sito di Hackademy 21
    <span class="px-1">&middot;</span>
    v{{ \Laravel\Nova\Nova::version() }}
</p>

<x-layouts.app
    title="{{ $article->title }}"
    description="{{ $article->getPreview() }}"
>

@push('styles')

<link href="{{$article->url()}}" rel="canonical"/>

<style>

</style>
@endpush

<div class="container">

    <header class="row py-5">

        <div class="col-12 text-center">

            <h1>{{ $article->title }}</h1>

            <p>
                @foreach($article->tags as $tag)
                <span class="badge badge-primary px-2 py-1 mr-1">{{ $tag->name }}</span>
                @endforeach
            </p>

            <figure>
                <img src="{{ Storage::url($article->img) }}" alt="{{ $article->title }}" class="img-fluid" width="100%">
            </figure>

        </div>

    </header>

    <section class="row py-5">

        @foreach($article->getMedia('gallery') as $item)
        <div class="col-12 col-sm-6 col-md-4">
            <figure>
                <img src="{{ $item->getUrl('thumb') }}" alt="{{ $article->title }}" class="img-fluid" width="100%">
            </figure>
        </div>
        @endforeach

    </section>

    <section class="row py-5">

        <div class="col-12">
            <div>{!! $article->description !!}</div>
        </div>

    </section>

</div>

@push('scripts')
<style>

</style>
@endpush

</x-layouts.app>
<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Waynestate\Nova\CKEditor;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Spatie\TagsField\Tags;

use Jackabox\DuplicateField\DuplicateField;
use Eminiarts\Tabs\Tabs;
use Eminiarts\Tabs\TabsOnEdit;

class Article extends Resource
{
    use TabsOnEdit;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Article::class;

    public static $group = 'Contenuti';

    public static function label() {
        return __('Articles');
    }

    public static function singularLabel() {
        return __('Article');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            (new Tabs('Tabs', [

                'Info'  => [
                    ID::make(__('ID'), 'id')->sortable(),

                    BelongsTo::make('Autore', 'user', User::class)
                        ->searchable()
                        ->nullable(),
        
                    Text::make(__('Titolo'), 'title')
                        ->rules('required')
                        ->sortable(),
        
                    CKEditor::make(__('Descrizione'), 'description')
                        ->hideFromIndex()
                        ->alwaysShow(),
        
                    DateTime::make(__('Data'), 'created_at')
                        ->format('DD/MM/YYYY')
                        ->sortable(),
        
                    Boolean::make(__('Pubblicato'), 'published'),
                ],

                'Media' => [
                    Tags::make('Tags'),
        
                    Avatar::make(__('Immagine'), 'img')
                        ->disk('public')
                        ->path('/articoli'),
        
                    Images::make('Gallery', 'gallery')
                        ->enableExistingMedia()
                        ->conversionOnIndexView('thumb')
                        ->rules('max:2'),
                ],
                
            ]))->withToolbar(),

            DuplicateField::make('Duplicate')
                ->withMeta([
                    'resource' => 'articles', // resource url
                    'model' => 'App\Models\Article', // model path
                    'id' => $this->id, // id of record
                    'relations' => [], // an array of any relations to load (nullable).
                    'except' => [], // an array of fields to not replicate (nullable).
                    'override' => ['published' => false] // an array of fields and values which will be set on the modal after duplicating (nullable).
                ])
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),
        ];
    }

    public static function indexQuery(NovaRequest $request, $query) {
        if($request->user()->isAdmin()) {
            return $query;
        } else {
            return $query->where('user_id', $request->user()->id);
        }
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

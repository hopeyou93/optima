
@extends('layouts.app')


@section('content')
<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-3">
                <div class="ads">
                    <h1 class="py-5 px-5 text-main">Ricerca quel che fa per te!</h1>
                    <div class="input-group pl-3">
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2 ml-5" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Ricerca</button>
                          </form>
                    </div>
                    <p class="text-main text-center py-2">Ricerca il tuo desiderio!</p>
                </div>
            </div>
        </div>
    </header>




    @endsection

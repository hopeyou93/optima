
@extends('layouts.app')


@section('content')
<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="ads shadow-lg">
                    <div class="col-md-12"></div>
                    <h1 class="py-4 px-5 text-main">Ricerca quel che fa per te!</h1>
                    <div class="input-group ">
                        <form class="form-inline">
                            <input class="form-control mr-sm-2 ml-5" type="search" placeholder="Cerca" aria-label="Search">
                            <button class="btn btn-outline-primary my-2" type="submit">Ricerca</button>
                          </form>
                    </div>
                    <p class="text-main pl-5 py-2">Ricerca il tuo desiderio!</p>
                </div>
            </div>
        </div>
    </header>




    @endsection
